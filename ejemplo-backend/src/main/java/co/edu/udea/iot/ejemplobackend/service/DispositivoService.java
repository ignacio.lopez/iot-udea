package co.edu.udea.iot.ejemplobackend.service;

import co.edu.udea.iot.ejemplobackend.exception.DataDuplicatedException;
import co.edu.udea.iot.ejemplobackend.exception.DataNotFoundException;
import co.edu.udea.iot.ejemplobackend.model.Dispositivo;
import co.edu.udea.iot.ejemplobackend.repository.DispositivoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DispositivoService {

    private DispositivoRepository dispositivoRepository;

    public DispositivoService(DispositivoRepository dispositivoRepository) {
        this.dispositivoRepository = dispositivoRepository;
    }

    public Dispositivo consultarDispositivo(String codigo) {
        Optional<Dispositivo> dispositivoOptional = this.dispositivoRepository.findById(codigo);
        if (dispositivoOptional.isPresent()) {
            return dispositivoOptional.get();
        }
       throw new DataNotFoundException(String.format("No se encuentra un dispositivo con el código: %s", codigo));
    }

    public List<Dispositivo> consultarDispositivos() {
        return this.dispositivoRepository.findAll();
    }

    public Dispositivo crearDispositivo(Dispositivo dispositivo) {
        Optional<Dispositivo> dispositivoOptional = this.dispositivoRepository.findById(dispositivo.getCodigo());
        if(dispositivoOptional.isPresent()){
            throw new DataDuplicatedException(String.format("Ya existe un dispositivo con el código: %s", dispositivo.getCodigo()));
        }
        return this.dispositivoRepository.save(dispositivo);
    }
}
