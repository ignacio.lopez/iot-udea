package co.edu.udea.iot.ejemplobackend.exception;

public class DataDuplicatedException extends RuntimeException {

    public DataDuplicatedException(String message) {
        super(message);
    }

}
