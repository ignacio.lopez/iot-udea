package co.edu.udea.iot.ejemplobackend.exception;

public class BusinessException extends RuntimeException {

    public BusinessException(String msg) {
        super(msg);
    }

}
