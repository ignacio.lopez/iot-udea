package co.edu.udea.iot.ejemplobackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjemploBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(EjemploBackendApplication.class, args);
	}

}
