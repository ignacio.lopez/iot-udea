package co.edu.udea.iot.ejemplobackend.controller;


import co.edu.udea.iot.ejemplobackend.comm.MessageSender;
import co.edu.udea.iot.ejemplobackend.model.Dispositivo;
import co.edu.udea.iot.ejemplobackend.service.DispositivoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/dispositivos")
public class DispositivoController {

    private DispositivoService dispositivoService;

    @Autowired
    private MessageSender messageSender;

    public DispositivoController(DispositivoService dispositivoService) {
        this.dispositivoService = dispositivoService;
    }

    @Operation(summary = "Permite consultar un dispositivo con base en su código")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Se ha consultado el dispositivo con éxito",
                    content = @Content(schema = @Schema(implementation = Dispositivo.class))),
            @ApiResponse(responseCode = "404", description = "Dispositivo no encontrado")
    })
    @GetMapping("/{codigo}")
    public ResponseEntity<Dispositivo> consultarDispositivo(
            @Parameter(description = "código del dispositivo a consultar", required = true)
            @PathVariable String codigo) {
        return ResponseEntity.accepted().body(this.dispositivoService.consultarDispositivo(codigo));
    }

    @Operation(summary = "Permite consultar todos los dispositivos almacenados")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Se han consultado los dispositivos con éxito",
                    content = @Content(schema = @Schema(implementation = Dispositivo.class)))
    })
    @GetMapping
    public ResponseEntity<List<Dispositivo>> consultarDispositivos() {
        return ResponseEntity.accepted().body(this.dispositivoService.consultarDispositivos());
    }

    @Operation(summary = "Permite crear un nuevo dispositivo")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Se ha creado el dispositivo con éxito",
                    content = @Content(schema = @Schema(implementation = Dispositivo.class)))
    })
    @PostMapping
    public ResponseEntity<Dispositivo> crearDispositivo(
            @Parameter(description = "Dispositivo a almacenar. No puede ser null o vacío", required = true, schema = @Schema(implementation = Dispositivo.class))
            @RequestBody Dispositivo dispositivo) {
        return ResponseEntity.accepted().body(this.dispositivoService.crearDispositivo(dispositivo));
    }


    @PostMapping("/mensajes/{codigo}/{mensaje}")
    public ResponseEntity enviarMensaje(@PathVariable String codigo, @PathVariable String mensaje){
        this.messageSender.send("device_"+codigo+"_inbound", mensaje.getBytes());
        return ResponseEntity.ok().build();
    }



}
