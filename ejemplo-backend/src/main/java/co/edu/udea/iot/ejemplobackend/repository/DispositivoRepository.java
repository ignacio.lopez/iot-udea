package co.edu.udea.iot.ejemplobackend.repository;

import co.edu.udea.iot.ejemplobackend.model.Dispositivo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DispositivoRepository extends JpaRepository<Dispositivo, String> {


}
