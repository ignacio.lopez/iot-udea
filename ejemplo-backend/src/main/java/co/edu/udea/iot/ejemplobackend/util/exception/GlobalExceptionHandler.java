package co.edu.udea.iot.ejemplobackend.util.exception;

import co.edu.udea.iot.ejemplobackend.exception.BusinessException;
import co.edu.udea.iot.ejemplobackend.exception.DataDuplicatedException;
import co.edu.udea.iot.ejemplobackend.exception.DataNotFoundException;
import co.edu.udea.iot.ejemplobackend.util.exception.msg.ResponseMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class GlobalExceptionHandler {

    private static Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<ResponseMessage> handleBusinessException(HttpServletRequest request, BusinessException be) {
        logger.error(request.getRequestURL().toString(), be);
        ResponseMessage responseMessage = new ResponseMessage(be.getMessage());
        return ResponseEntity.badRequest().body(responseMessage);
    }

    @ExceptionHandler(DataNotFoundException.class)
    public ResponseEntity handleBusinessException(HttpServletRequest request, DataNotFoundException dnfe) {
        logger.error(request.getRequestURL().toString(), dnfe);
        ResponseMessage responseMessage = new ResponseMessage(dnfe.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseMessage);
    }

    @ExceptionHandler(DataDuplicatedException.class)
    public ResponseEntity<ResponseMessage> handleBusinessException(HttpServletRequest request, DataDuplicatedException dde) {
        logger.error(request.getRequestURL().toString(), dde);
        ResponseMessage responseMessage = new ResponseMessage(dde.getMessage());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(responseMessage);
    }

}
