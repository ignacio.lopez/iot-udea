package co.edu.udea.iot.ejemplobackend.comm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@RabbitListener(queues = "devices_outbound")
@Component
public class MessageReceiver {

    private static Logger logger = LoggerFactory.getLogger(MessageReceiver.class);


    @RabbitHandler
    public void processIncomingMessage(byte[] message) {
        String mensaje = new String(message);
        logger.info(String.format("Se ha recibido: %s ", mensaje));
    }
}
