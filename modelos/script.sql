
CREATE TABLE public.dispositivos (
                codigo VARCHAR NOT NULL,
                tipo VARCHAR NOT NULL,
                estado VARCHAR NOT NULL,
                nombre VARCHAR NOT NULL,
                CONSTRAINT dispositivos_pk PRIMARY KEY (codigo)
);
