import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";

const urlBase = 'http://localhost:8080/api/dispositivos'

@Injectable({
  providedIn: 'root'
})
export class DispositivoService {

  constructor(private httpClient: HttpClient) {
  }

  public enviarMensaje(codigo: string, mensaje: string): Observable<any> {
    return this.httpClient.post(`${urlBase}/mensajes/${codigo}/${mensaje}`, null);
  }

}
