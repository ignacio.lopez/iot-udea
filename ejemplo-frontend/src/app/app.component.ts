import {Component, OnDestroy, OnInit} from '@angular/core';
import {DispositivoService} from "../shared/service/dispositivo.service";
import {StompService} from "@stomp/ng2-stompjs";

import {Message} from '@stomp/stompjs';
import {Observable, Subscription} from "rxjs";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  title = 'Ejemplo front iot';

  codigo = '';
  mensaje = '';

  messages: Observable<Message>;
  subscription: Subscription;

  messagesList: Array<String> = [];

  constructor(private dispositivoService: DispositivoService,
              private stompService: StompService,
              private toastrService: ToastrService) {
  }

  ngOnInit(): void {
    //se usa /topic para especificar que la suscripción será a un tópico y no a una cola
    this.messages = this.stompService.subscribe('/topic/devices_outbound.device_1');

    this.subscription = this.messages.subscribe(this.on_message);


  }

  public on_message = (message: Message) => {
    const body = message.body;
    this.messagesList.push(body + '\n');
  }


  enviarMensaje() {

    this.dispositivoService.enviarMensaje(this.codigo, this.mensaje).subscribe(
      respuesta => {
        this.toastrService.success(`Mensaje enviado: ${this.mensaje}`, 'Comunicación');
        this.codigo = '';
        this.mensaje = '';
        console.log(respuesta);
      }
    )
  }

  ngOnDestroy(): void {
    this.stompService.disconnect();
    this.subscription.unsubscribe();

  }


}
